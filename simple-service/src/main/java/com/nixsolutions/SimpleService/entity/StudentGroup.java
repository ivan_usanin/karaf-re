package com.nixsolutions.SimpleService.entity;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class StudentGroup {

	private Long studentGroupId;
	private String studentGroupName;
	
	public StudentGroup() {
	}

	public StudentGroup(Long studentGroupId, String studentGroupName) {
		this.studentGroupId = studentGroupId;
		this.studentGroupName = studentGroupName;
	}

	public Long getStudentGroupId() {		
		return studentGroupId;
	}

	public void setStudentGroupId(Long studentGroupId) {
        this.studentGroupId = studentGroupId;
	}
	
	public String getStudentGroupName(){
		return studentGroupName;
	}

	public void setStudentGroupName(String studentGroupName){
		this.studentGroupName = studentGroupName;
	}

	@Override
	public String toString() {
		return "StudentGroup [studentGroupId=" + studentGroupId + ", studentGroupName=" + studentGroupName + "]";
	}

}
